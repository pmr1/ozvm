/*
 * Z88BodyGui.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:gstrube@gmail.com">Gunther Strube</A>
 *
 */
package com.jira.cambridgez88.ozvm;

import java.awt.Color;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 * This window is only used to hold the Z88 Keyboard and Car Slot, when OZvm is
 * using Double Screen size.
 *
 * @author Gunther Strube, gstrube@gmail.com
 */
public class Z88BodyGui extends JFrame {

    public Z88BodyGui(RubberKeyboard kb, Slots sp) {
        super();

        setBackground(Color.BLACK);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        setIconImage(new ImageIcon(this.getClass().getResource("/pixel/title.gif")).getImage());

        getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        getContentPane().add(kb);
        getContentPane().add(sp);

        pack(); // update the application UI
        setVisible(true);

        // Position the "external" keyboard + slots below the big screen
        JFrame gui = OZvm.getInstance().getGui();

        setLocation(gui.getX()+(gui.getWidth()-this.getWidth())/2, gui.getY()+gui.getHeight()+4);
    }
}
