/*
 * CommandLine.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:gstrube@gmail.com">Gunther Strube</A>
 *
 */
package com.jira.cambridgez88.ozvm;

import com.jira.cambridgez88.ozvm.datastructures.ApplicationDor;
import com.jira.cambridgez88.ozvm.datastructures.ApplicationInfo;
import com.jira.cambridgez88.ozvm.filecard.FileArea;
import com.jira.cambridgez88.ozvm.filecard.FileAreaExhaustedException;
import com.jira.cambridgez88.ozvm.filecard.FileAreaNotFoundException;
import com.jira.cambridgez88.ozvm.filecard.FileEntry;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.ListIterator;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * The OZvm debug command line.
 */
public class CommandLine {

    private static final class singletonContainer {
        static final CommandLine singleton = new CommandLine();
    }

    public static CommandLine getInstance() {
        return singletonContainer.singleton;
    }

    private static final String illegalArgumentMessage = "Illegal Argument";
    private boolean logZ80instructions;
    private Blink blink;
    private Z80Processor z80;
    /**
     * The Z88 disassembly engine
     */
    private Dz dz;
    /**
     * The Breakpoint manager
     */
    private Breakpoints breakPointManager;

    /**
     * The Watchpoint manager
     */
    private Watchpoints watchpointManager;

    /**
     * Access the Z88 memory model
     */
    private Memory memory;

    // The collected watchpoint command line arguments.
    private class WatchpointCmdArguments {
        public int argc;
        public int addrStart;               // -1 if not specified
        public int addrEnd;                 // -1 if not specified
        public ArrayList<String> wpCmds;    // null if not specified

        public WatchpointCmdArguments() {
            argc = 0;
            addrStart = -1;
            addrEnd = -1;
            wpCmds = null;
        }
    }

    /**
     * Constructor
     */
    private CommandLine() {

        blink = Z88.getInstance().getBlink();
        z80 = Z88.getInstance().getProcessor();
        memory = Z88.getInstance().getMemory();

        dz = Dz.getInstance();
        breakPointManager = z80.getBreakpoints();
        watchpointManager = z80.getWatchpoints();

        displayCmdOutput("Type 'help' + ENTER for available debugging commands");
        displayCmdOutput("Type 'run' + ENTER to continue executing Z88");
    }

    private void displayCmdOutput(String msg) {
        JTextArea commandOutput = DebugGui.getInstance().getCmdlineOutputArea();
        commandOutput.append(msg + "\n");
    }

    private void cmdHelp() {
        displayCmdOutput("--------------------------------------------------------------------------------------");
        displayCmdOutput("Command Reference:");
        displayCmdOutput("--------------------------------------------------------------------------------------");
        displayCmdOutput("quitvm                     - quit OZvm desktop application");
        displayCmdOutput("loadvm [filename]          - Load z88 default 'boot.z88' or specified snapshot filename");
        displayCmdOutput("savevm [filename]          - Create a snapshot of Z88 to default 'boot.z88' or filename");
        displayCmdOutput("fcd1-3 [cmd]               - File Card Management - direct memory API without flap INT");
        displayCmdOutput("       format              - Format file area (all contents erased)");
        displayCmdOutput("       cardhdr             - Create/update file area header (no format)");
        displayCmdOutput("       reclaim             - Reclaim deleted file space");
        displayCmdOutput("       del filename        - Mark file as deleted in file area");
        displayCmdOutput("       ipf hostfile        - Import file from desktop into file area");
        displayCmdOutput("       xpf file hostdir    - Export file entry to desktop directory");
        displayCmdOutput("       ipd hostdir         - Import all files from desktop directory into file area");
        displayCmdOutput("       xpc hostdir         - Export all file entries to desktop directory");
        displayCmdOutput("ldc filename extaddr       - Load code from file into bank+offset");
        displayCmdOutput("dumpslot X [-b][pathname]  - Dump slot X as 'slotX.epr' file, optionally as bank files");
        displayCmdOutput("app name                   - Display DOR details for specified application name");
        displayCmdOutput("apps                       - List all available static application DOR's");
        displayCmdOutput("");
        displayCmdOutput("wpcl                       - Clear all watchpoints");
        displayCmdOutput("wwpd [addr] [addr-range]   - Display/toggle Z80 register dump point at Write watchpoint");
        displayCmdOutput("wrpd [addr] [addr-range]   - Display/toggle Z80 register dump point at Read watchpoint");
        displayCmdOutput("wpd [addr] [addr-range]    - Display/toggle Z80 register dump point at Read+Write watchpoint");
        displayCmdOutput("wwp [addr] [addr-range]    - Display/toggle Z80 Write watchpoints (local/ext. address)");
        displayCmdOutput("wrp [addr] [addr-range]    - Display/toggle Z80 Read watchpoints (local/ext. address)");
        displayCmdOutput("wp [addr] [addr-range]     - Display/toggle Z80 Read+Write watchpoints (local/ext. address)");
        displayCmdOutput("log                        - Enable/disable Z80 instruction execution logging to file");
        displayCmdOutput("bozd                       - Display/toggle Z80 register dump at OZ System call");
        displayCmdOutput("boz                        - Enable/disable Z80 Break at OZ System call");
        displayCmdOutput("bpcl                       - Clear all bp/bpd breakpoints.");
        displayCmdOutput("bpd [addr]                 - Display/toggle Z80 register dump point (local/ext. address)");
        displayCmdOutput("bp [addr]                  - Display/toggle Z80 breakpoint (local/ext. address)");
        displayCmdOutput("");
        displayCmdOutput("sbr [w]                    - Display/set Blink Screen Base Register");
        displayCmdOutput("pb0-3 [w]                  - Display/set Blink PB0,PB1,PB2 & PB3 16bit registers");
        displayCmdOutput("tim0-4 [b]                 - Display/set Blink CLOCK registers");
        displayCmdOutput("tmk,tack,tsta [b]          - Display/set Blink TMK,TACK & TSTA registers");
        displayCmdOutput("kbd [w]                    - Display KBD matrix, set Blink KBD 16bit register");
        displayCmdOutput("com,int,sta,ack [b]        - Display/set Blink COM,INT,STA,ACK registers");
        displayCmdOutput("sr0-3 [b]                  - Display/set Blink Segment Register SR0,SR1,SR2,SR3 bindings");
        displayCmdOutput("sr                         - Display all Blink Segment Register bank bindings");
        displayCmdOutput("bl                         - Display Blink register contents");
        displayCmdOutput("rg                         - Display current Z80 registers");
        displayCmdOutput("");
        displayCmdOutput("BC DE HL    [w]            - Display or set 16bit registers, eg HL 40CC (set register)");
        displayCmdOutput("BC' DE' HL' [w]");
        displayCmdOutput("IX IY SP PC [w]");
        displayCmdOutput("EI/DI                      - Enable/Disable INT flip/flop");
        displayCmdOutput("f/F                        - Display current Z80 flag register");
        displayCmdOutput("FZ FC FN                   - Display or set Flag Register bit, FZ 1 enables Zero flag");
        displayCmdOutput("FS FV FH");
        displayCmdOutput("A B C D E H L I R    [b]   - Display or set 8bit registers, eg B 40 (set register)");
        displayCmdOutput("A' B' C' D' E' H' L' [b]");
        displayCmdOutput("");
        displayCmdOutput("wb extaddr {b}             - Write one or more bytes to ext. addr, eg. wb 201ffe 00 40");
        displayCmdOutput("m [addr]                   - View memory at default PC, or <addr>");
        displayCmdOutput("                             local address = 2000, ext. addr = 803fc0 (bank+offset)");
        displayCmdOutput("dz [addr]                  - Disassemble at default PC or <addr>");
        displayCmdOutput("                             local address = 3f00, ext. addr = 073fc0 (bank+offset)");
        displayCmdOutput("");
        displayCmdOutput("stop                       - Stop Z80 CPU (also via F5); Enters command/step mode");
        displayCmdOutput("run                        - leave debugging mode and run Z80 CPU");
        displayCmdOutput("z                          - Trace CALL/RST Z80 subroutine until RET or breakpoint");
        displayCmdOutput(".                          - Single step next Z80 instruction");
        displayCmdOutput("cls                        - Clear debug command output window");
    }

    public void parseCommandLine(String cmdLineText) {
        JTextField cmdLineInp = DebugGui.getInstance().getCmdLineInputArea();

        cmdLineText = cmdLineText.replaceAll("[(]", " ( ");
        cmdLineText = cmdLineText.replaceAll("[)]", " ) ");
        cmdLineText = cmdLineText.replaceAll("[;]", " ; ");

        String[] cmdLineTokens = cmdLineText.split(" ");
        int arg;

        if (cmdLineTokens[0].compareToIgnoreCase("help") == 0) {
            cmdHelp();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("savevm") == 0) {
            SaveRestoreVM srVm = new SaveRestoreVM();
            String vmFileName = OZvm.defaultVmFile;

            if (cmdLineTokens.length > 1) {
                vmFileName = cmdLineTokens[1];
                if (vmFileName.toLowerCase().lastIndexOf(".z88") == -1) {
                    vmFileName += ".z88"; // '.z88' extension was missing.
                }
            }

            try {
                if (Z88.getInstance().getProcessorThread() == null) {
                    srVm.storeSnapShot(vmFileName, false);
                    displayCmdOutput("Snapshot successfully saved to " + vmFileName);
                } else {
                    displayCmdOutput("Snapshot can only be saved when Z88 is not running.");
                }
            } catch (IOException e) {
                displayCmdOutput("Saving snapshot failed.");
            }
        }

        if (cmdLineTokens[0].compareToIgnoreCase("loadvm") == 0) {
            SaveRestoreVM srVm = new SaveRestoreVM();
            String vmFileName = OZvm.defaultVmFile;

            if (cmdLineTokens.length > 1) {
                vmFileName = cmdLineTokens[1];
                if (vmFileName.toLowerCase().lastIndexOf(".z88") == -1) {
                    vmFileName += ".z88"; // '.z88' extension was missing.
                }
            }

            if (Z88.getInstance().getProcessorThread() == null) {
                try {
                    boolean autorun = srVm.loadSnapShot(vmFileName);
                    displayCmdOutput("Snapshot successfully installed from " + vmFileName);
                    if (autorun == true) {
                        Z88.getInstance().runZ80Cpu();
                        Z88.getInstance().getDisplay().grabFocus(); // default keyboard input focus to the Z88
                    } else {
                        cmdlineFirstSingleStep();
                    }
                } catch (IOException e) {
                    // loading of snapshot failed - define a default Z88 system
                    // as fall back plan.
                    displayCmdOutput("Installation of snapshot failed. Z88 preset to default system.");
                    memory.setDefaultSystem();
                    z80.reset();
                    blink.resetBlinkRegisters();
                }
            } else {
                displayCmdOutput("Snapshot can only be installed when Z88 is not running.");
            }
        }

        if (cmdLineTokens[0].compareToIgnoreCase("cls") == 0) {
            DebugGui.getInstance().clearCmdOutputArea();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("apps") == 0) {
            ApplicationInfo appInfo = new ApplicationInfo();
            for (int slot = 0; slot < 4; slot++) {
                ListIterator appList = appInfo.getApplications(slot);
                if (appList != null) {
                    displayCmdOutput("Slot " + slot + ":");
                    while (appList.hasNext()) {
                        ApplicationDor appDor = (ApplicationDor) appList.next();
                        displayCmdOutput(appDor.getAppName() + ": DOR = " + Dz.extAddrToHex(appDor.getThisApp(), true) + ", Entry = " + Dz.extAddrToHex(appDor.getEntryPoint(), true));
                    }
                }
            }
        }

        if (cmdLineTokens[0].compareToIgnoreCase("app") == 0) {
            ApplicationInfo appInfo = new ApplicationInfo();
            boolean found = false;
            ListIterator appList;
            ApplicationDor appDor;
            int slot;

            if (cmdLineTokens.length == 2) {
                for (slot = 0; slot < 4; slot++) {
                    appList = appInfo.getApplications(slot);
                    if (appList != null) {
                        while (appList.hasNext()) {
                            appDor = (ApplicationDor) appList.next();
                            if (appDor.getAppName().contains(cmdLineTokens[1])) {
                                found = true;
                                displayCmdOutput("DOR information, " + appDor.getAppName() + " ( []" + appDor.getKeyLetter() + " ) :");
                                displayCmdOutput("DOR pointer: " + Dz.extAddrToHex(appDor.getThisApp(), true));
                                displayCmdOutput("Execution Entry: " + Dz.extAddrToHex(appDor.getEntryPoint(), true) + ", bindings: "
                                        + "S0=" + Dz.byteToHex(appDor.getSegment0BankBinding(), true) + ", "
                                        + "S1=" + Dz.byteToHex(appDor.getSegment1BankBinding(), true) + ", "
                                        + "S2=" + Dz.byteToHex(appDor.getSegment2BankBinding(), true) + ", "
                                        + "S3=" + Dz.byteToHex(appDor.getSegment3BankBinding(), true));
                                displayCmdOutput("Mth: Topics=" + Dz.extAddrToHex(appDor.getTopics(), true) + ", "
                                        + "Commands=" + Dz.extAddrToHex(appDor.getCommands(), true) + ", "
                                        + "Help=" + Dz.extAddrToHex(appDor.getHelp(), true) + ", "
                                        + "Tokens=" + Dz.extAddrToHex(appDor.getTokens(), true));
                            }
                        }
                    }
                }
            } else {
                displayCmdOutput("Specify Application name (missing). Available apps are:");
                for (slot = 0; slot < 4; slot++) {
                    appList = appInfo.getApplications(slot);
                    if (appList != null) {
                        while (appList.hasNext()) {
                            appDor = (ApplicationDor) appList.next();
                            displayCmdOutput(appDor.getAppName());
                        }
                    }
                }
            }
        }

        if ( (cmdLineTokens[0].compareToIgnoreCase("run") == 0) | (cmdLineTokens[0].compareToIgnoreCase("x") == 0)) {
            // always do a single step first before running Z80 engine again
            // (avoid that an IM 1 comes just before the first instruction to be executed)
            if (Z88.getInstance().getProcessorThread() == null) {
                z80.singleStepZ80();
            }
            if (Z88.getInstance().runZ80Cpu() == false) {
                displayCmdOutput("Z88 is already running.");
            } else {
                // input fields are no longer editable
                DebugGui.getInstance().lockZ88MachinePanel(false);

                // make sure that keyboard focus is available for Z88 (screen)
                OZvm.getInstance().getGui().toFront();
                Z88.getInstance().getDisplay().grabFocus();
            }
        }

        if (cmdLineTokens[0].compareToIgnoreCase("stop") == 0) {
            if (Z88.getInstance().getProcessor().isZ80ThreadRunning() == true) {
                // exit the Z80Processor.run() method and enter single step debug mode
                Z88.getInstance().getProcessor().stopZ80Execution();

                // if thread is sleeping, there is nothing to stop... so force a wake-up, so Z80 can stop
                Z88.getInstance().getBlink().awakeZ80();

                DebugGui.getInstance().activateDebugCommandLine(); // Activate Debug Command Line Window...
                CommandLine.getInstance().cmdlineFirstSingleStep();
                DebugGui.getInstance().lockZ88MachinePanel(true);
            } else {
                displayCmdOutput("Z88 is already stopped.");
            }
        }

        if (cmdLineTokens[0].compareToIgnoreCase("quitvm") == 0) {
            System.exit(0);
        }

        if (cmdLineTokens[0].compareToIgnoreCase("di") == 0) {
            if (Z88.getInstance().getProcessorThread() != null) {
                displayCmdOutput("Interrupt state cannot be edited while Z88 is running.");
                return;
            } else {
                z80.IFF1(false);
                z80.IFF2(false);
                displayCmdOutput("Maskable interrupts disabled.");
            }
        }

        if (cmdLineTokens[0].compareToIgnoreCase("ei") == 0) {
            if (Z88.getInstance().getProcessorThread() != null) {
                displayCmdOutput("Interrupt state cannot be edited while Z88 is running.");
                return;
            } else {
                z80.IFF1(true);
                z80.IFF2(true);
                displayCmdOutput("Maskable interrupts enabled.");
            }
        }

        if (cmdLineTokens[0].compareToIgnoreCase("log") == 0) {
            if (z80.izZ80Logged() == true) {
                logZ80instructions = false;
                z80.setZ80Logging(logZ80instructions);
                z80.flushZ80LogCache(); // flush the Z80 instruction log cache, if there's anything in it.
                displayCmdOutput("Z80 Instruction logging disabled.");
            } else {
                logZ80instructions = true;
                z80.setZ80Logging(logZ80instructions);
                displayCmdOutput("Z80 Instruction logging enabled.");
            }
        }

        if (cmdLineTokens[0].compareTo(".") == 0) {
            if (Z88.getInstance().getProcessorThread() != null) {
                displayCmdOutput("Z88 is running - single stepping ignored.");
                return;
            }

            z80.singleStepZ80();        // single stepping (no interrupts running)...
            DebugGui.getInstance().refreshZ88HardwareInfo();
            displayCmdOutput(Z88Info.dzPcStatus(z80.PC()));
            cmdLineInp.setText(Dz.getNextStepCommand());
            cmdLineInp.setCaretPosition(cmdLineInp.getDocument().getLength());
            cmdLineInp.selectAll();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("z") == 0) {
            if (Z88.getInstance().getProcessorThread() != null) {
                displayCmdOutput("Z88 is running - subroutine execution ignored.");
                return;
            } else {
                // do we really have a subroutine at PC?
                if (Dz.getNextStepCommand().compareTo("z") == 0) {
                    int nextInstrAddress = blink.decodeLocalAddress(dz.getNextInstrAddress(z80.PC()));
                    // always do a single step first before running Z80 engine again
                    // (avoid that an IM 1 comes just before the first instruction to be executed)
                    z80.singleStepZ80();

                    if (breakPointManager.isCreated(nextInstrAddress) == true) {
                        // there's already a breakpoint at that location...
                        Z88.getInstance().runZ80Cpu();
                    } else {
                        // set a temporary breakpoint at next instruction and automatically remove it when the engine stops...
                        Z88.getInstance().runZ80Cpu(nextInstrAddress);
                    }
                } else {
                    // no, do a single step command
                    z80.singleStepZ80();        // single stepping (no interrupts running)...
                    DebugGui.getInstance().refreshZ88HardwareInfo();

                    cmdLineInp.setText(Dz.getNextStepCommand());
                    cmdLineInp.setCaretPosition(cmdLineInp.getDocument().getLength());
                    cmdLineInp.selectAll();
                    displayCmdOutput(Z88Info.dzPcStatus(z80.PC()));
                }
            }
        }

        if (cmdLineTokens[0].compareToIgnoreCase("fcd1") == 0
                | cmdLineTokens[0].compareToIgnoreCase("fcd2") == 0
                | cmdLineTokens[0].compareToIgnoreCase("fcd3") == 0) {
            fcdCommandline(cmdLineTokens);
        }

        if (cmdLineTokens[0].compareToIgnoreCase("dz") == 0) {
            dzCommandline(cmdLineTokens);
            displayCmdOutput("");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("m") == 0) {
            viewMemory(cmdLineTokens);
            displayCmdOutput("");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("bl") == 0) {
            displayCmdOutput(Z88Info.blinkRegisterDump());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("sr") == 0) {
            displayCmdOutput(Z88Info.bankBindingInfo() + "\n");
            displayCmdOutput("");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("rg") == 0) {
            displayCmdOutput("\n" + Z88Info.z80RegisterInfo());
            displayCmdOutput("");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("boz") == 0) {
            breakPointManager.toggleBreakOzCall();
            displayCmdOutput("Break at OZ CALL: " + Boolean.toString(breakPointManager.isBreakOzCallActive()));
        }

        if (cmdLineTokens[0].compareToIgnoreCase("bozd") == 0) {
            breakPointManager.toggleDisplayOzCall();
            displayCmdOutput("Display OZ CALL: " + Boolean.toString(breakPointManager.isDisplayOzCallActive()));
        }

        if (cmdLineTokens[0].compareToIgnoreCase("bp") == 0) {
            try {
                bpCommandline(cmdLineTokens);
                displayCmdOutput("");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        if (cmdLineTokens[0].compareToIgnoreCase("bpcl") == 0) {
            breakPointManager.clearBreakpoints();
            breakPointManager.removeBreakPoints();
            displayCmdOutput("All breakpoints cleared.");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("bpd") == 0) {
            try {
                bpdCommandline(cmdLineTokens);
                displayCmdOutput("");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        if (cmdLineTokens[0].compareToIgnoreCase("wp") == 0) {
            // Read/Write Watchpoint
            WatchpointCmdArguments wpArgs;

            try {
                wpArgs = wpCommandline(cmdLineTokens);

                switch (wpArgs.argc) {
                    case 1:
                        watchpointManager.toggleWatchpoint(wpArgs.addrStart);
                        break;
                    case 2:
                        if (wpArgs.wpCmds == null) {
                            for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++)
                                watchpointManager.toggleWatchpoint(addr);
                        } else {
                            watchpointManager.toggleWatchpoint(wpArgs.addrStart, wpArgs.wpCmds);
                        }
                        break;
                    case 3:
                        for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++) {
                            watchpointManager.toggleWatchpoint(addr, wpArgs.wpCmds);
                        }
                        break;
                }
            } catch (IOException ex) {}

            displayCmdOutput(watchpointManager.displayWatchpoints());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("wpd") == 0) {
            // Read/Write Watchpoint Display (dont stop)
            WatchpointCmdArguments wpArgs;

            try {
                wpArgs = wpCommandline(cmdLineTokens);

                switch (wpArgs.argc) {
                    case 1:
                        watchpointManager.toggleWatchpoint(wpArgs.addrStart, false);
                        break;
                    case 2:
                        if (wpArgs.wpCmds == null) {
                            // runtime display doesn't execute debug commands...
                            for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++)
                                watchpointManager.toggleWatchpoint(addr, false);
                        }
                        break;
                }
            } catch (IOException ex) {}

            displayCmdOutput(watchpointManager.displayWatchpoints());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("wrp") == 0) {
            // Read Watchpoint
            WatchpointCmdArguments wpArgs;

            try {
                wpArgs = wpCommandline(cmdLineTokens);

                switch (wpArgs.argc) {
                    case 1:
                        watchpointManager.toggleReadWatchpoint(wpArgs.addrStart);
                        break;
                    case 2:
                        if (wpArgs.wpCmds == null) {
                            for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++)
                                watchpointManager.toggleReadWatchpoint(addr);
                        } else {
                            watchpointManager.toggleReadWatchpoint(wpArgs.addrStart, wpArgs.wpCmds);
                        }
                        break;
                    case 3:
                        for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++) {
                            watchpointManager.toggleReadWatchpoint(addr, wpArgs.wpCmds);
                        }
                        break;
                }
            } catch (IOException ex) {}

            displayCmdOutput(watchpointManager.displayWatchpoints());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("wrpd") == 0) {
            // Read Watchpoint Display (dont stop)
            WatchpointCmdArguments wpArgs;

            try {
                wpArgs = wpCommandline(cmdLineTokens);

                switch (wpArgs.argc) {
                    case 1:
                        watchpointManager.toggleReadWatchpoint(wpArgs.addrStart, false);
                        break;
                    case 2:
                        if (wpArgs.wpCmds == null) {
                            // runtime display doesn't execute debug commands...
                            for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++)
                                watchpointManager.toggleReadWatchpoint(addr, false);
                        }
                        break;
                }
            } catch (IOException ex) {}

            displayCmdOutput(watchpointManager.displayWatchpoints());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("wwp") == 0) {
            // Write Watchpoint
            WatchpointCmdArguments wpArgs;

            try {
                wpArgs = wpCommandline(cmdLineTokens);

                switch (wpArgs.argc) {
                    case 1:
                        watchpointManager.toggleWriteWatchpoint(wpArgs.addrStart);
                        break;
                    case 2:
                        if (wpArgs.wpCmds == null) {
                            for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++)
                                watchpointManager.toggleWriteWatchpoint(addr);
                        } else {
                            watchpointManager.toggleWriteWatchpoint(wpArgs.addrStart, wpArgs.wpCmds);
                        }
                        break;
                    case 3:
                        for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++) {
                            watchpointManager.toggleWriteWatchpoint(addr, wpArgs.wpCmds);
                        }
                        break;
                }
            } catch (IOException ex) {}

            displayCmdOutput(watchpointManager.displayWatchpoints());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("wwpd") == 0) {
            // Write Watchpoint Display (don't stop)
            WatchpointCmdArguments wpArgs;

            try {
                wpArgs = wpCommandline(cmdLineTokens);

                switch (wpArgs.argc) {
                    case 1:
                        watchpointManager.toggleWriteWatchpoint(wpArgs.addrStart, false);
                        break;
                    case 2:
                        if (wpArgs.wpCmds == null) {
                            // runtime display doesn't execute debug commands...
                            for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++)
                                watchpointManager.toggleWriteWatchpoint(addr, false);
                        }
                        break;
                }
            } catch (IOException ex) {}

            displayCmdOutput(watchpointManager.displayWatchpoints());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("wpcl") == 0) {
            watchpointManager.clearWatchpoints();
            watchpointManager.removeWatchpoints();
            displayCmdOutput("All watchpoints cleared.");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("wb") == 0) {
            try {
                putByte(cmdLineTokens);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        if (cmdLineTokens[0].compareToIgnoreCase("ldc") == 0) {
            if (cmdLineTokens.length == 3) {
                try {
                    int extAddress = Integer.parseInt(cmdLineTokens[2], 16);
                    int bank = (extAddress >>> 16) & 0xFF;
                    int offset = extAddress & 0x3FFF;
                    Bank b = memory.getBank(bank);

                    memory.loadBankBinary(b, offset, new File(cmdLineTokens[1]));
                    displayCmdOutput("File image '" + cmdLineTokens[1] + "' loaded at " + cmdLineTokens[2] + ".");
                } catch (IOException e) {
                    displayCmdOutput("Couldn't load file image at ext.address: '" + e.getMessage() + "'");
                }

            } else {
                displayCmdOutput("incorrect arguments");
            }
        }

        if (cmdLineTokens[0].compareToIgnoreCase("dumpslot") == 0) {
            boolean exportAsBanks = false;
            int slotNumber;
            String dumpFilename;
            String dumpDir;

            if (cmdLineTokens.length >= 2) {
                slotNumber = Integer.parseInt(cmdLineTokens[1]);
                dumpFilename = "slot" + cmdLineTokens[1] + ".epr";
                dumpDir = System.getProperty("user.home");

                if (slotNumber > 0) {
                    if (cmdLineTokens.length == 2) {
                        // "dumpslot X"
                        // dump the specified slot as a complete file
                        // using a default "slotX.epr" filename
                    } else if (cmdLineTokens.length == 3) {
                        // "dumpslot X -b" or "dumpslot X filename"
                        if (cmdLineTokens[2].compareToIgnoreCase("-b") == 0) {
                            dumpFilename = "slot" + cmdLineTokens[1] + "bank";
                            exportAsBanks = true;
                        } else {
                            dumpFilename = cmdLineTokens[2];
                        }
                    } else if (cmdLineTokens.length == 4) {
                        // "dumpslot X -b base-filename"
                        // base filename (with optional path) for filename.bankNo
                        if (cmdLineTokens[2].compareToIgnoreCase("-b") == 0) {
                            exportAsBanks = true;
                        }

                        File fl = new File(cmdLineTokens[3]);
                        if (fl.isDirectory() == true) {
                            dumpDir = fl.getAbsolutePath();
                            dumpFilename = "slot" + cmdLineTokens[1] + "bank";
                        } else {
                            if (fl.getParent() != null) {
                                dumpDir = new File(fl.getParent()).getAbsolutePath();
                            }

                            dumpFilename = fl.getName();
                        }
                    }

                    if (memory.isSlotEmpty(slotNumber) == false) {
                        try {
                            memory.dumpSlot(slotNumber, exportAsBanks, dumpDir, dumpFilename);
                            displayCmdOutput("Slot was dumped successfully to " + dumpDir);
                        } catch (FileNotFoundException e1) {
                            displayCmdOutput("Couldn't create file(s)!");
                        } catch (IOException e1) {
                            displayCmdOutput("I/O error while dumping slot!");
                        }
                    } else {
                        displayCmdOutput("Slot is empty!");
                    }
                }

            } else {
                displayCmdOutput("Arguments missing!");
            }
        }

        if (cmdLineTokens[0].compareToIgnoreCase("com") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setCom(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkComInfo());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("int") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setInt(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkIntInfo());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("sta") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setSta(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkStaInfo());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("kbd") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    Z88.getInstance().getKeyboard().setKeyRow(arg >>> 8, arg & 0xFF);
                }
            }
            displayCmdOutput(Z88.getInstance().getKeyboard().getKbdMatrixSymbolically());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("ack") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setAck(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkStaInfo());  // ACK affects STA
        }

        if (cmdLineTokens[0].compareToIgnoreCase("epr") == 0) {
            // Not yet implemented
        }

        if (cmdLineTokens[0].compareToIgnoreCase("tsta") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTsta(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkTstaInfo());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("tack") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTack(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkTstaInfo()); // TACK affects TSTA
        }

        if (cmdLineTokens[0].compareToIgnoreCase("tmk") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTmk(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkTmkInfo());
        }


        if (cmdLineTokens[0].compareToIgnoreCase("pb0") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setPb0(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkScreenInfo());
        }
        if (cmdLineTokens[0].compareToIgnoreCase("pb1") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setPb1(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkScreenInfo());
        }
        if (cmdLineTokens[0].compareToIgnoreCase("pb2") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setPb2(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkScreenInfo());
        }
        if (cmdLineTokens[0].compareToIgnoreCase("pb3") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setPb3(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkScreenInfo());
        }
        if (cmdLineTokens[0].compareToIgnoreCase("sbr") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setSbr(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkScreenInfo());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("sr0") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setSegmentBank(0, arg);
                }
            }
            displayCmdOutput(Z88Info.blinkSegmentsInfo());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("sr1") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setSegmentBank(1, arg);
                }
            }
            displayCmdOutput(Z88Info.blinkSegmentsInfo());
        }
        if (cmdLineTokens[0].compareToIgnoreCase("sr2") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setSegmentBank(2, arg);
                }
            }
            displayCmdOutput(Z88Info.blinkSegmentsInfo());
        }
        if (cmdLineTokens[0].compareToIgnoreCase("sr3") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setSegmentBank(3, arg);
                }
            }
            displayCmdOutput(Z88Info.blinkSegmentsInfo());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("tim0") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTim0(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkTimersInfo());
        }
        if (cmdLineTokens[0].compareToIgnoreCase("tim1") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTim1(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkTimersInfo());
        }
        if (cmdLineTokens[0].compareToIgnoreCase("tim2") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTim2(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkTimersInfo());
        }
        if (cmdLineTokens[0].compareToIgnoreCase("tim3") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTim3(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkTimersInfo());
        }
        if (cmdLineTokens[0].compareToIgnoreCase("tim4") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTim4(arg);
                }
            }
            displayCmdOutput(Z88Info.blinkTimersInfo());
        }

        if (cmdLineTokens[0].compareToIgnoreCase("f") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change F flag while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.F(arg);
                }
            }
            displayCmdOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("f'") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change alternate A register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.ex_af_af();
                    z80.F(arg);
                    z80.ex_af_af();
                }
            }
            z80.ex_af_af();
            displayCmdOutput("F'=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");
            z80.ex_af_af();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("fz") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change Zero flag while Z88 is running!");
                    return;
                }
                if (StringEval.toInteger(cmdLineTokens[1]) == 0) {
                    z80.fZ = false;
                } else {
                    z80.fZ = true;
                }
            } else {
                // toggle/invert flag status
                z80.fZ = !z80.fZ;
            }
            displayCmdOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("fc") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change Carry flag while Z88 is running!");
                    return;
                }
                if (StringEval.toInteger(cmdLineTokens[1]) == 0) {
                    z80.fC = false;
                } else {
                    z80.fC = true;
                }
            } else {
                // toggle/invert flag status
                z80.fC = !z80.fC;
            }
            displayCmdOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("fs") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change Sign flag while Z88 is running!");
                    return;
                }
                if (StringEval.toInteger(cmdLineTokens[1]) == 0) {
                    z80.fS = false;
                } else {
                    z80.fS = true;
                }
            } else {
                // toggle/invert flag status
                z80.fS = !z80.fS;
            }
            displayCmdOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("fh") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change Half Carry flag while Z88 is running!");
                    return;
                }
                if (StringEval.toInteger(cmdLineTokens[1]) == 0) {
                    z80.fH = false;
                } else {
                    z80.fH = true;
                }
            } else {
                // toggle/invert flag status
                z80.fH = !z80.fH;
            }
            displayCmdOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("fn") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change Add./Sub. flag while Z88 is running!");
                    return;
                }
                if (StringEval.toInteger(cmdLineTokens[1]) == 0) {
                    z80.fN = false;
                } else {
                    z80.fN = true;
                }
            } else {
                // toggle/invert flag status
                z80.fN = !z80.fN;
            }
            displayCmdOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("fv") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change Parity flag while Z88 is running!");
                    return;
                }
                if (StringEval.toInteger(cmdLineTokens[1]) == 0) {
                    z80.fPV = false;
                } else {
                    z80.fPV = true;
                }
            } else {
                // toggle/invert flag status
                z80.fPV = !z80.fPV;
            }
            displayCmdOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("a") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change A register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.A(arg);
                }
            }
            displayCmdOutput("A=" + Dz.byteToHex(z80.A(), true) + " (" + Dz.byteToBin(z80.A(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("a'") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change alternate A register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.ex_af_af();
                    z80.A(arg);
                    z80.ex_af_af();
                }
            }
            z80.ex_af_af();
            displayCmdOutput("A'=" + Dz.byteToHex(z80.A(), true) + " (" + Dz.byteToBin(z80.A(), true) + ")");
            z80.ex_af_af();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("b") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change B register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.B(arg);
                }
            }
            displayCmdOutput("B=" + Dz.byteToHex(z80.B(), true) + " (" + Dz.byteToBin(z80.B(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("c") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change C register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.C(arg);
                }
            }
            displayCmdOutput("C=" + Dz.byteToHex(z80.C(), true) + " (" + Dz.byteToBin(z80.C(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("b'") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change alternate B register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.exx();
                    z80.B(arg);
                    z80.exx();
                }
            }
            z80.exx();
            displayCmdOutput("B'=" + Dz.byteToHex(z80.B(), true) + " (" + Dz.byteToBin(z80.B(), true) + ")");
            z80.exx();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("c'") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change alternate C register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.exx();
                    z80.C(Integer.parseInt(cmdLineTokens[1], 16) & 0xFF);
                    z80.exx();
                }
            }
            z80.exx();
            displayCmdOutput("C'=" + Dz.byteToHex(z80.C(), true) + " (" + Dz.byteToBin(z80.C(), true) + ")");
            z80.exx();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("bc") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change BC register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.BC(arg);
                }
            }
            displayCmdOutput("BC=" + Dz.addrToHex(z80.BC(), true) + " (" + z80.BC() + "d)");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("bc'") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change alternate BC register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.exx();
                    z80.BC(arg);
                    z80.exx();
                }
            }
            z80.exx();
            displayCmdOutput("BC'=" + Dz.addrToHex(z80.BC(), true) + " (" + z80.BC() + "d)");
            z80.exx();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("d") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change D register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.D(arg);
                }
            }
            displayCmdOutput("D=" + Dz.byteToHex(z80.D(), true) + " (" + Dz.byteToBin(z80.D(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("e") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change E register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.E(arg);
                }
            }
            displayCmdOutput("E=" + Dz.byteToHex(z80.E(), true) + " (" + Dz.byteToBin(z80.E(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("d'") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change alternate D register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.exx();
                    z80.D(arg);
                    z80.exx();
                }
            }
            z80.exx();
            displayCmdOutput("D'=" + Dz.byteToHex(z80.D(), true) + " (" + Dz.byteToBin(z80.D(), true) + ")");
            z80.exx();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("e'") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change alternate E register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.exx();
                    z80.E(arg);
                    z80.exx();
                }
            }
            z80.exx();
            displayCmdOutput("E'=" + Dz.byteToHex(z80.E(), true) + " (" + Dz.byteToBin(z80.E(), true) + ")");
            z80.exx();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("de") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change DE register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.DE(arg);
                }
            }
            displayCmdOutput("DE=" + Dz.addrToHex(z80.DE(), true) + " (" + z80.DE() + "d)");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("de'") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change alternate DE register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.exx();
                    z80.DE(arg);
                    z80.exx();
                }
            }
            z80.exx();
            displayCmdOutput("DE'=" + Dz.addrToHex(z80.DE(), true) + " (" + z80.DE() + "d)");
            z80.exx();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("h") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change H register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.H(arg);
                }
            }
            displayCmdOutput("H=" + Dz.byteToHex(z80.H(), true) + " (" + Dz.byteToBin(z80.H(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("l") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change L register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.L(arg);
                }
            }
            displayCmdOutput("L=" + Dz.byteToHex(z80.L(), true) + " (" + Dz.byteToBin(z80.L(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("h'") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change alternate H register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.exx();
                    z80.H(arg);
                    z80.exx();
                }
            }
            z80.exx();
            displayCmdOutput("H'=" + Dz.byteToHex(z80.H(), true) + " (" + Dz.byteToBin(z80.H(), true) + ")");
            z80.exx();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("l'") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change alternate L register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.exx();
                    z80.L(arg);
                    z80.exx();
                }
            }
            z80.exx();
            displayCmdOutput("L'=" + Dz.byteToHex(z80.L(), true) + " (" + Dz.byteToBin(z80.L(), true) + ")");
            z80.exx();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("hl") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change HL register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.HL(arg);
                }
            }
            displayCmdOutput("HL=" + Dz.addrToHex(z80.HL(), true) + " (" + z80.HL() + "d)");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("hl'") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change alternate HL register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.exx();
                    z80.HL(arg);
                    z80.exx();
                }
            }
            z80.exx();
            displayCmdOutput("HL'=" + Dz.addrToHex(z80.HL(), true) + " (" + z80.HL() + "d)");
            z80.exx();
        }

        if (cmdLineTokens[0].compareToIgnoreCase("i") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change I register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.I(arg);
                }
            }
            displayCmdOutput("I=" + Dz.byteToHex(z80.I(), true) + " (" + Dz.byteToBin(z80.I(), true) + ")");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("ix") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change IX register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.IX(arg);
                }
            }
            displayCmdOutput("IX=" + Dz.addrToHex(z80.IX(), true) + " (" + z80.IX() + "d)");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("iy") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change IY register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.IY(arg);
                }
            }
            displayCmdOutput("IY=" + Dz.addrToHex(z80.IY(), true) + " (" + z80.IY() + "d)");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("sp") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change SP register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.SP(arg);
                }
            }
            displayCmdOutput("SP=" + Dz.addrToHex(z80.SP(), true) + " (" + z80.SP() + "d)");
        }

        if (cmdLineTokens[0].compareToIgnoreCase("pc") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change PC register while Z88 is running!");
                    return;
                }
                if ((cmdLineTokens[1].startsWith("+") == true) | (cmdLineTokens[1].startsWith("-") == true)) {
                    arg = z80.PC() + StringEval.toInteger(cmdLineTokens[1]);
                } else {
                    arg = StringEval.toInteger(cmdLineTokens[1]);
                }
                if (arg == -1 | arg > 65535) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.PC(arg);
                }
            }
            displayCmdOutput(Z88Info.dzPcStatus(z80.PC()));
        }

        if (cmdLineTokens[0].compareToIgnoreCase("r") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    displayCmdOutput("Cannot change R register while Z88 is running!");
                    return;
                }
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    displayCmdOutput(illegalArgumentMessage);
                    return;
                } else {
                    z80.R(arg);
                }
            }
            displayCmdOutput("R=" + Dz.byteToHex(z80.R(), true) + " (" + Dz.byteToBin(z80.R(), true) + ")");
        }

        DebugGui.getInstance().refreshZ88HardwareInfo();
    }

    /**
     * Display current Z80 instruction and simple register dump, and preset a
     * single stepping or subroutine debug command.
     */
    public void cmdlineFirstSingleStep() {
        JTextField cmdLineInp = DebugGui.getInstance().getCmdLineInputArea();

        displayCmdOutput(Z88Info.dzPcStatus(z80.PC()));
        DebugGui.getInstance().refreshZ88HardwareInfo();
        cmdLineInp.setText(Dz.getNextStepCommand());
        cmdLineInp.setCaretPosition(cmdLineInp.getDocument().getLength());
        cmdLineInp.selectAll();
        cmdLineInp.grabFocus();    // Z88 is stopped, get focus to debug command line.
    }

    private void fcdCommandline(String[] cmdLineTokens) {
        try {
            if (cmdLineTokens.length == 1) {
                // no sub-commands are specified, just list file area contents...
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                ListIterator fileEntries = fa.getFileEntries();

                if (fileEntries == null) {
                    displayCmdOutput("File area is empty.");
                } else {
                    displayCmdOutput("File area:");
                    while (fileEntries.hasNext()) {
                        FileEntry fe = (FileEntry) fileEntries.next();
                        displayCmdOutput(fe.getFileName()
                                + ((fe.isDeleted() == true) ? " [d]" : "")
                                + ", size=" + fe.getFileLength() + " bytes"
                                + ", entry=" + Dz.extAddrToHex(fe.getFileEntryPtr(), true));
                    }
                }
            } else if (cmdLineTokens.length == 2 & cmdLineTokens[1].compareToIgnoreCase("format") == 0) {
                // create or (re)format file area
                if (FileArea.create(cmdLineTokens[0].getBytes()[3] - 48, true) == true) {
                    displayCmdOutput("File area were created/formatted.");
                } else {
                    displayCmdOutput("File area could not be created/formatted.");
                }

            } else if (cmdLineTokens.length == 2 & cmdLineTokens[1].compareToIgnoreCase("cardhdr") == 0) {
                // just create a file area header
                if (FileArea.create(cmdLineTokens[0].getBytes()[3] - 48, false) == true) {
                    displayCmdOutput("File area header were created.");
                } else {
                    displayCmdOutput("File area header could not be created.");
                }

            } else if (cmdLineTokens.length == 2 & cmdLineTokens[1].compareToIgnoreCase("reclaim") == 0) {
                // reclaim deleted file space
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                fa.reclaimDeletedFileSpace();
                displayCmdOutput("Deleted files have been removed from file area.");

            } else if (cmdLineTokens.length == 3 & cmdLineTokens[1].compareToIgnoreCase("del") == 0) {
                // mark file as deleted
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                if (fa.markAsDeleted(cmdLineTokens[2]) == true) {
                    displayCmdOutput("File was marked as deleted.");
                } else {
                    displayCmdOutput("File not found.");
                }

            } else if (cmdLineTokens.length == 3 & cmdLineTokens[1].compareToIgnoreCase("ipf") == 0) {
                // import file from host file system into file area...
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                fa.importHostFile(new File(cmdLineTokens[2]));
                displayCmdOutput("File " + cmdLineTokens[2] + " was successfully imported.");

            } else if (cmdLineTokens.length == 3 & cmdLineTokens[1].compareToIgnoreCase("ipd") == 0) {
                // import all files from host file system directory into file area...
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                fa.importHostFiles(new File(cmdLineTokens[2]));
                displayCmdOutput("Directory '" + cmdLineTokens[2] + "' was successfully imported.");

            } else if (cmdLineTokens.length == 3 & cmdLineTokens[1].compareToIgnoreCase("xpc") == 0) {
                // export all files from file area to directory on host file system..
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                ListIterator fileEntries = fa.getFileEntries();
                if (fa.getActiveFileCount() == 0) {
                    displayCmdOutput("No files available to export.");
                } else {
                    while (fileEntries.hasNext()) {
                        FileEntry fe = (FileEntry) fileEntries.next();

                        if (fe.isDeleted() == false) {
                            // strip the "oz" path of the filename
                            String hostFileName = fe.getFileName();
                            hostFileName = hostFileName.substring(hostFileName.lastIndexOf("/") + 1);
                            // and build a complete file name for the host file system
                            hostFileName = cmdLineTokens[2] + File.separator + hostFileName;

                            // create a new file in specified host directory
                            RandomAccessFile expFile = new RandomAccessFile(hostFileName, "rw");
                            expFile.write(fe.getFileImage()); // export file image to host file system
                            expFile.close();

                            displayCmdOutput("Exported " + fe.getFileName() + " to " + hostFileName);
                        }
                    }
                }

            } else if (cmdLineTokens.length == 4 & cmdLineTokens[1].compareToIgnoreCase("xpf") == 0) {
                // export file from file area to directory on host file system
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                FileEntry fe = fa.getFileEntry(cmdLineTokens[2]);
                if (fe == null) {
                    displayCmdOutput("File not found.");
                } else {
                    // strip the "oz" path of the filename
                    String hostFileName = fe.getFileName();
                    hostFileName = hostFileName.substring(hostFileName.lastIndexOf("/") + 1);
                    // and build a complete file name for the host file system
                    hostFileName = cmdLineTokens[3] + File.separator + hostFileName;

                    // create a new file in specified host directory
                    RandomAccessFile expFile = new RandomAccessFile(hostFileName, "rw");
                    expFile.write(fe.getFileImage()); // export file image to host file system
                    expFile.close();

                    displayCmdOutput("Exported " + fe.getFileName() + " to " + hostFileName);
                }
            } else {
                displayCmdOutput("Unknown file card command or missing arguments.");
            }
        } catch (FileAreaNotFoundException e) {
            displayCmdOutput("No file area found in slot.");
        } catch (FileAreaExhaustedException e) {
            displayCmdOutput("No more room in file area. One or several files could not be imported.");
        } catch (IOException e) {
            displayCmdOutput("I/O error occurred during import/export of files.");
        }
    }

    private WatchpointCmdArguments wpCommandline(String[] cmdLineTokens) throws IOException {
        WatchpointCmdArguments wpArgs = new WatchpointCmdArguments();

        if (cmdLineTokens.length >= 2) {
            // first argument is startaddress (or maybe just a single address)
            wpArgs.argc = 1;
            wpArgs.addrStart = Dz.stringAddr2Integer(cmdLineTokens[1]);
        }

        if (cmdLineTokens.length >= 3) {
            // 2nd argument either end address or "+" length specifier or debug commands
            wpArgs.argc++;
            int tokenIdx = 2;

            if (cmdLineTokens[tokenIdx].compareTo("(") == 0) {
                // parse rest of command line for commands to execute at watch point
                wpArgs.wpCmds = parseDebugCmds(cmdLineTokens, tokenIdx);

                return wpArgs;
            }

            if (cmdLineTokens[tokenIdx].startsWith("+") == true) {
               // length specifier
               wpArgs.addrEnd = wpArgs.addrStart + Integer.parseInt(cmdLineTokens[tokenIdx++].substring(1)) - 1;
            } else {
                // 3rd argument is an end-range address
                wpArgs.addrEnd = Dz.stringAddr2Integer(cmdLineTokens[tokenIdx++]);
            }

            if (tokenIdx < cmdLineTokens.length) {
                if ( cmdLineTokens[tokenIdx].compareTo("(") == 0 ) {
                    // parse rest of command line for commands to execute at watch point
                    wpArgs.argc++;
                    wpArgs.wpCmds = parseDebugCmds(cmdLineTokens, tokenIdx);
                }
            }
        }

        return wpArgs;
    }


    private ArrayList<String> parseDebugCmds(String[] cmdLineTokens, int tokenIdx) {
        ArrayList<String> cmds = new ArrayList<String>();
        String cmd = "";

        while (tokenIdx < cmdLineTokens.length & cmdLineTokens[tokenIdx].compareTo("(") != 0) {
            tokenIdx++;
        }

        tokenIdx++; // point at first token of commands
        while (tokenIdx < cmdLineTokens.length) {

            if (cmdLineTokens[tokenIdx].length() > 0) {
                if ((cmdLineTokens[tokenIdx].compareTo(";") == 0 | cmdLineTokens[tokenIdx].compareTo(")") == 0) & cmd.length() > 0) {
                    // command separator or end of commands found, add current command string to list of commands
                    cmd = cmd.trim();
                    cmds.add(cmd);
                    cmd = "";
                } else {
                    cmd += cmdLineTokens[tokenIdx] + " ";
                }
            }

            tokenIdx++;
        }

        return cmds;
    }

    private void bpCommandline(String[] cmdLineTokens) throws IOException {
        int bpAddress;

        if (cmdLineTokens.length >= 2) {
            bpAddress = Dz.stringAddr2Integer(cmdLineTokens[1]);

            if (cmdLineTokens.length == 2) {
                breakPointManager.toggleBreakpoint(bpAddress, true);
            } else {
                // parse rest of command line for commands to execute at break point
                breakPointManager.toggleBreakpoint(bpAddress, parseDebugCmds(cmdLineTokens, 2));
            }
            displayCmdOutput(breakPointManager.displayBreakpoints());
        }

        if (cmdLineTokens.length == 1) {
            // no arguments, use PC in current bank binding
            displayCmdOutput(breakPointManager.displayBreakpoints());
        }
    }

    private void bpdCommandline(String[] cmdLineTokens) throws IOException {
        int bpAddress;

        if (cmdLineTokens.length == 2) {
            bpAddress = Dz.stringAddr2Integer(cmdLineTokens[1]);

            breakPointManager.toggleBreakpoint(bpAddress, false);
            displayCmdOutput(breakPointManager.displayBreakpoints());
        }

        if (cmdLineTokens.length == 1) {
            // no arguments, use PC in current bank binding
            displayCmdOutput(breakPointManager.displayBreakpoints());
        }
    }

    private void dzCommandline(String[] cmdLineTokens) {
        boolean localAddressing = true;
        int dzAddr = 0, dzBank = 0;
        StringBuffer dzLine = new StringBuffer(64);
        JTextField cmdLineInp =  DebugGui.getInstance().getCmdLineInputArea();

        if (cmdLineTokens.length == 2) {
            // one argument; the local Z80 64K address or a compact 24bit extended address
            dzAddr = Integer.parseInt(cmdLineTokens[1], 16);
            if (dzAddr > 65535) {
                dzBank = (dzAddr >>> 16) & 0xFF;
                dzAddr &= 0xFFFF;   // bank offset (with simulated segment addressing)
                localAddressing = false;
            } else {
                if (cmdLineTokens[1].length() == 6) {
                    // bank defined as '00'
                    dzBank = 0;
                    localAddressing = false;
                } else {
                    localAddressing = true;
                }
            }
        } else {
            if (cmdLineTokens.length == 1) {
                // no arguments, use PC in current bank binding (use local addressing)...
                dzAddr = z80.PC();
                localAddressing = true;
            } else {
                displayCmdOutput("Illegal argument.");
                return;
            }
        }

        if (localAddressing == true) {
            for (int dzLines = 0; dzLines < 16; dzLines++) {
                int origAddr = dzAddr;
                dzAddr = dz.getInstrAscii(dzLine, dzAddr, false, true);
                displayCmdOutput(Dz.addrToHex(origAddr, false) + " (" + Dz.extAddrToHex(blink.decodeLocalAddress(origAddr), false).toString() + ") " + dzLine.toString());
            }

            DebugGui.getInstance().getCmdLineInputArea().setText("dz " + Dz.addrToHex(dzAddr, false));
        } else {
            // extended addressing
            for (int dzLines = 0; dzLines < 16; dzLines++) {
                int origAddr = dzAddr;
                dzAddr = dz.getInstrAscii(dzLine, dzAddr, dzAddr & 0x3fff, dzBank, false, true);
                displayCmdOutput(Dz.extAddrToHex((dzBank << 16) | origAddr, false) + " " + dzLine);
            }

            cmdLineInp.setText("dz " + Dz.extAddrToHex((dzBank << 16) | dzAddr, false));
        }
        cmdLineInp.setCaretPosition(cmdLineInp.getDocument().getLength());
        cmdLineInp.selectAll();
    }

    private int getMemoryAscii(StringBuffer memLine, int memAddr) {
        int memHex, memAscii;

        memLine.delete(0, 255);
        for (memHex = memAddr; memHex < memAddr + 16; memHex++) {
            memLine.append(Dz.byteToHex(blink.readByte(memHex), false)).append(" ");
        }

        for (memAscii = memAddr; memAscii < memAddr + 16; memAscii++) {
            int b = blink.readByte(memAscii);
            memLine.append((b >= 32 && b <= 127) ? Character.toString((char) b) : ".");
        }

        return memAscii;
    }

    private int getMemoryAscii(StringBuffer memLine, int memAddr, int memBank) {
        int memHex, memAscii;

        memLine.delete(0, 255);
        for (memHex = memAddr; memHex < memAddr + 16; memHex++) {
            memLine.append(Dz.byteToHex(memory.getByte(memHex, memBank), false)).append(" ");
        }

        for (memAscii = memAddr; memAscii < memAddr + 16; memAscii++) {
            int b = memory.getByte(memAscii, memBank);
            memLine.append((b >= 32 && b <= 127) ? Character.toString((char) b) : ".");
        }

        return memAscii;
    }

    private void putByte(String[] cmdLineTokens) throws IOException {
        int argByte[], memAddress, memBank, aByte;

        if (cmdLineTokens.length >= 3 & cmdLineTokens.length <= 18) {
            memAddress = Integer.parseInt(cmdLineTokens[1], 16);
            memBank = (memAddress >>> 16) & 0xFF;
            memAddress &= 0xFFFF;
            argByte = new int[cmdLineTokens.length - 2];
            for (aByte = 0; aByte < cmdLineTokens.length - 2; aByte++) {
                argByte[aByte] = Integer.parseInt(cmdLineTokens[2 + aByte], 16);
            }
        } else {
            displayCmdOutput("Illegal argument(s).");
            return;
        }

        StringBuffer memLine = new StringBuffer(256);
        getMemoryAscii(memLine, memAddress, memBank);
        displayCmdOutput("Before:\n" + memLine);
        for (aByte = 0; aByte < cmdLineTokens.length - 2; aByte++) {
            memory.setByte(memAddress + aByte, memBank, argByte[aByte]);
        }

        getMemoryAscii(memLine, memAddress, memBank);
        displayCmdOutput("After:\n" + memLine);
    }

    private void viewMemory(String[] cmdLineTokens) {
        int memAddr = 0, bankNo = 0, offset = 0;

        if (cmdLineTokens.length == 2) {
            // one argument; the local Z80 64K address or 24bit compact ext. address
            memAddr = Dz.stringAddr2Integer(cmdLineTokens[1]);
        } else {
            if (cmdLineTokens.length == 1) {
                // no arguments, use PC in current bank binding (use local addressing)...
                memAddr = blink.decodeLocalAddress(z80.PC());
            } else {
                displayCmdOutput("Illegal argument.");
                return;
            }
        }

        bankNo = (memAddr >>> 16) & 0xFF;
        offset = memAddr & 0x3FFF;

        Z88.getInstance().getMemory().getBank(bankNo).editMemory(offset, bankNo);
    }
}
